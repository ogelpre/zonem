.PHONY: build clean html install lint test verify

PYTHON=python3 -Wall

all: verify build

build: html

clean:
	git clean -X -d -f

html: README.html TODO.html

install:
ifeq ($(shell id -u), 0)
	pip install .
else
	pip install --user .
endif

lint:
	pycodestyle --config .pycodestyle setup.py zonem tests
	flake8 --config=.flake8 setup.py zonem tests
	pylint --rcfile=.pylintrc setup.py zonem tests

test:
	git --version
	${PYTHON} -m tests

verify: lint test

README.html:
	${PYTHON} -m markdown2 README.md > README.html

TODO.html:
	${PYTHON} -m markdown2 TODO.md > TODO.html
