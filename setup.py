"""setup.py for zone manager."""

import re
import setuptools


with open("zonem/__init__.py", "rt", encoding="utf8") as stream:
    VERSION = re.search(r"__version__ = \'(.*?)\'", stream.read()).group(1)

with open("README.md", "rt", encoding="utf8") as stream:
    LONG_DESCRIPTION = stream.read()

with open("LICENSE", "rt", encoding="utf8") as stream:
    LICENSE = stream.read()

setuptools.setup(
    name='zonem-ogelpre',
    version=VERSION,
    author="Benedikt Neuffer",
    author_email="ogelpre@itfriend.de",
    description="Manage zone files in git.",
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    url="https://gitlab.com/ogelpre/zonem",
    license=LICENSE,
    packages=setuptools.find_packages(),
    install_requires=[
        'click',
        'click-log',
        'dnspython<2',
        'pyyaml'
    ],
    entry_points={
        'console_scripts': [
            'zonem = zonem.__main__:main',
        ],
    },
    classifiers=[
        # prevents accidential upload
        'Private :: Do not upload',
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
