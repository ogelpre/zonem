"""Create PTR for hosts"""

import pathlib
import unittest

import dns.name

import zonem.config
import zonem.dns
import zonem.post_processing
import zonem.utils

BASE_PATH = pathlib.Path(__file__).parent

ZONES_VALID = BASE_PATH.parent.joinpath('test_dns').joinpath('zones_valid')

ZONES_INVALID_MISSING_REVERSE_ZONE = BASE_PATH.joinpath('zones_invalid_missing_reverse_zone')


class PTRGeneratorTestCase(unittest.TestCase):
    """test zonem.post_process.ptr.PTRGenerator"""

    def test_ptr_generator(self):
        """test zonem.post_process.ptr.PTRGenerator"""

        zonem.utils.singleton.reset()

        host_list = zonem.post_processing.ptr.HostList()
        config = zonem.config.config.Config()
        config.base_dir = ZONES_VALID
        zone_list = zonem.dns.zone.collect_zones(ZONES_VALID, config)
        self.assertEqual(host_list['2001:db8::2'][0], dns.name.from_text('web-1.example.com.'))

        ptr_generator = zonem.post_processing.ptr.PTRGenerator(zone_list)
        ptr_generator.run()

        zone = zone_list[dns.name.from_text('8.b.d.0.1.0.0.2.ip6.arpa')]
        name = dns.name.from_text('2.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.8.b.d.0.1.0.0.2.ip6.arpa')
        name_relativized = name.relativize(zone.origin)
        node = zone.nodes.get(name_relativized)
        rdataset = node.get_rdataset(dns.rdataclass.IN, dns.rdatatype.PTR)
        self.assertEqual(rdataset[0].target, dns.name.from_text('web-1.example.com.'))

    def test_ptr_invalid(self):
        """test zonem.post_process.ptr.PTRGenerator with invalid ptr"""

        zonem.utils.singleton.reset()

        host_list = zonem.post_processing.ptr.HostList()
        config = zonem.config.config.Config()
        config.base_dir = ZONES_INVALID_MISSING_REVERSE_ZONE
        zone_list = zonem.dns.zone.collect_zones(ZONES_INVALID_MISSING_REVERSE_ZONE, config)
        self.assertEqual(host_list['2001:db8::2'][0], dns.name.from_text('web-1.example.com.'))

        ptr_generator = zonem.post_processing.ptr.PTRGenerator(zone_list)
        with self.assertRaises(ValueError, msg='PTR generation failed, because zone not found for \
2.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.8.b.d.0.1.0.0.2.ip6.arpa..'):
            ptr_generator.run()
