"""Create PTR for hosts"""

import pathlib
import unittest

import zonem.config
import zonem.dns
import zonem.post_processing
import zonem.utils

BASE_PATH = pathlib.Path(__file__).parent

ZONES_INVALID_DOUBLE_XNAME = BASE_PATH.joinpath('zones_invalid_double_xname')
ZONES_INVALID_RR_AND_CNAME = BASE_PATH.joinpath('zones_invalid_rr_and_cname')
ZONES_INVALID_TARGET = BASE_PATH.joinpath('zones_invalid_target')
ZONES_INVALID_TARGET_NO_HOST = BASE_PATH.joinpath('zones_invalid_target_no_host')
ZONES_INVALID_EXTERNAL_TARGET = BASE_PATH.joinpath('zones_invalid_external_target')


class TargetValidatorTestCase(unittest.TestCase):
    """test zonem.post_process.target.Validator"""

    def test_double_xname_invalid(self):
        """test zonem.post_process.target.Validator with invalid cname and dname"""

        zonem.utils.singleton.reset()

        config = zonem.config.config.Config()
        config.base_dir = ZONES_INVALID_DOUBLE_XNAME
        zone_list = zonem.dns.zone.collect_zones(ZONES_INVALID_DOUBLE_XNAME, config)

        validator = zonem.post_processing.target.Validator(zone_list)
        with self.assertRaises(RuntimeError, msg='No other records are allowed beside CNAME or DNAME (www2.example.com.)'):
            validator.run(False)

    def test_rr_and_cname(self):
        """test zonem.post_process.target.Validator with cname and other rr"""

        zonem.utils.singleton.reset()

        config = zonem.config.config.Config()
        config.base_dir = ZONES_INVALID_RR_AND_CNAME
        zone_list = zonem.dns.zone.collect_zones(ZONES_INVALID_RR_AND_CNAME, config)

        validator = zonem.post_processing.target.Validator(zone_list)
        with self.assertRaises(RuntimeError, msg='No other records are allowed beside CNAME or DNAME (www2.example.com.)'):
            validator.run(False)

    def test_invalid_target(self):
        """test zonem.post_process.target.Validator with invalid target"""

        zonem.utils.singleton.reset()

        config = zonem.config.config.Config()
        config.base_dir = ZONES_INVALID_TARGET
        zone_list = zonem.dns.zone.collect_zones(ZONES_INVALID_TARGET, config)

        validator = zonem.post_processing.target.Validator(zone_list)
        with self.assertRaises(ValueError, msg='Target mail.example.com. for MX example.com. does not exist'):
            validator.run(False)

    def test_invalid_target_no_host(self):
        """test zonem.post_process.target.Validator with invalid target (no A/AAAA/CNAME/DNAME)"""

        zonem.utils.singleton.reset()

        config = zonem.config.config.Config()
        config.base_dir = ZONES_INVALID_TARGET_NO_HOST
        zone_list = zonem.dns.zone.collect_zones(ZONES_INVALID_TARGET_NO_HOST, config)

        validator = zonem.post_processing.target.Validator(zone_list)
        msg = 'Target xmpp-1.example.com. for SRV _xmpp-client._tcp.example.com. has no A, AAAA, CNAME or DNAME record'
        with self.assertRaises(ValueError, msg=msg):
            validator.run(False)

    def test_invalid_external_target(self):
        """test zonem.post_process.target.Validator with invalid external target"""

        zonem.utils.singleton.reset()

        config = zonem.config.config.Config()
        config.base_dir = ZONES_INVALID_EXTERNAL_TARGET
        zone_list = zonem.dns.zone.collect_zones(ZONES_INVALID_EXTERNAL_TARGET, config)

        validator = zonem.post_processing.target.Validator(zone_list)
        msg = 'External target 5aa8aedav6wauhaquahn3aiphuif3.ad5eibub3parahgh2eiz4je6axuu5. for MX example.com. does not exist'
        with self.assertRaises(ValueError, msg=msg):
            validator.run(True)
