"""DNS Zone"""

import pathlib
import unittest
import unittest.mock

import dns.name
import dns.zone
import zonem.config
import zonem.dns
import zonem.utils
import zonem.post_processing

BASE_PATH = pathlib.Path(__file__).parent

ZONES_VALID = BASE_PATH.joinpath('zones_valid')

ZONES_INVALID_SYMLINK = BASE_PATH.joinpath('zones_invalid_symlink')

ZONES_INVALID_HOST = BASE_PATH.joinpath('zones_invalid_host')


class ZoneWalkerTestCase(unittest.TestCase):
    """test zonem.dns.zone.Zonewalker"""

    def test_walk(self):
        """test walking over a directory"""

        zonem.utils.singleton.reset()

        base_dir = ZONES_VALID.joinpath('example.com')
        origin = dns.name.from_text('example.com')
        walker = zonem.dns.zone.ZoneWalker(base_dir, origin)

        reference = [
            ('example.com.',
             'TXT "v=spf1 ip6:2001:db8::2 ip4:192.0.2.2 ~all"\n',
             "{}/example.com/spf".format(ZONES_VALID)),
            ('www.example.com.',
             'AAAA 2001:db8::1\nA 192.0.2.1\n',
             "{}/example.com/webserver".format(ZONES_VALID)),
            ('staging.example.com.',
             'AAAA 2001:db8::1\nA 192.0.2.1\n',
             "{}/example.com/webserver".format(ZONES_VALID)),
            ('example.com.',
             'TXT "adobe-idp-site-verification=xyz"\n\n',
             "{}/example.com/adobe-idp-site-verification".format(ZONES_VALID)),
            ('mail.example.com.',
             'AAAA 2001:db8::4\nA 192.0.2.4\n',
             "{}/example.com/mail/host".format(ZONES_VALID)),
            ('example.com.',
             'SOA ns-1.example.com. hostmaster.example.com. 1 1 1 1 1\n',
             "{}/soa".format(ZONES_VALID)),
            ('example.com.',
             'AAAA 2001:db8::1\nA 192.0.2.1\n',
             "{}/example.com/webserver".format(ZONES_VALID)),
            ('example.com.',
             'CAA 0 issue "letsencrypt.org"\n'
             'CAA 0 issuewild ";"\n'
             'CAA 0 iodef "mailto:security@example.com"\n'
             'CAA 0 iodef "https://security.example.com/"\n',
             "{}/example.com/caa".format(ZONES_VALID)),
            ('_xmpp-client._tcp.example.com.',
             'SRV 5 0 5222 xmpp-1.example.net.\n',
             "{}/example.com/_tcp/_xmpp-client/xmpp".format(ZONES_VALID)),
            ('_xmpp-server._tcp.example.com.',
             'SRV 5 0 5269 xmpp-1.example.net.\n',
             "{}/example.com/_tcp/_xmpp-server/xmpp".format(ZONES_VALID)),
            ('example.com.',
             'TXT "MS=123"\n\n',
             "{}/example.com/microsoft-site-verification".format(ZONES_VALID)),
            ('example.com.',
             'NS ns-1\nNS ns-2.example.org.\n',
             "{}/example.com/ns".format(ZONES_VALID)),
            ('web-1.example.com.',
             'SSHFP 2 1 123456789abcdef67890123456789abcdef67890\n',
             "{}/example.com/web-1/sshfp".format(ZONES_VALID)),
            ('web-1.example.com.',
             'LOC 34 15 28 S 72 8 0 W -5m\n',
             "{}/example.com/web-1/loc".format(ZONES_VALID)),
            ('web-1.example.com.',
             'AAAA 2001:db8::2\nA 192.0.2.2\n',
             "{}/example.com/web-1/host".format(ZONES_VALID)),
            ('example.com.',
             'NAPTR 100 10 "s" "x-eduroam:radius.tls" "" _radsec._tcp.eduroam.de.\n',
             "{}/example.com/eduroam".format(ZONES_VALID)),
            ('example.com.',
             'TXT "google-site-verification=xxx"\nTXT "google-site-verification=yyy"\n\n',
             "{}/example.com/google-site-verfication".format(ZONES_VALID)),
            ('departement.example.com.',
             'DS 52037 1 1 378929E92D7DA04267EE87E802D75C5CA1B5D280\n',
             "{}/example.com/departement/ds".format(ZONES_VALID)),
            ('departement.example.com.',
             'NS ad.departement\n',
             "{}/example.com/departement/ns".format(ZONES_VALID)),
            ('ad.departement.example.com.',
             'AAAA 2001:db8::3\n',
             "{}/example.com/departement/ad/glue".format(ZONES_VALID)),
            ('example.com.',
             '1 TXT "low TTL for testing purpose"\n',
             "{}/example.com/low-ttl".format(ZONES_VALID)),
            ('example.com.',
             'TXT "comment" ; comment\n',
             "{}/example.com/comment".format(ZONES_VALID)),
            ('www2.example.com.',
             'CNAME @\n',
             "{}/example.com/www2/redirect".format(ZONES_VALID)),
            ('ns-1.example.com.',
             'AAAA 2001:db8::\nA 192.0.2.0\n',
             "{}/example.com/ns-1/host".format(ZONES_VALID)),
            ('example.com.',
             'MX 10 mail\nMX 100 mail.example.org.\n',
             "{}/example.com/mx".format(ZONES_VALID))]

        for label, data, filename in walker.walk():
            reference.remove((str(label), data, str(filename)))

        self.assertFalse(reference)


class ZoneReaderTestCase(unittest.TestCase):
    """test zonem.dns.zone.ZoneReader"""

    def test_read(self):
        """Read zone from path"""

        zonem.utils.singleton.reset()

        zone_data = (
            '@ 1 IN TXT "adobe-idp-site-verification=xyz"\n'
            '@ 1 IN TXT "comment"\n'
            '@ 1 IN TXT "google-site-verification=xxx"\n'
            '@ 1 IN TXT "google-site-verification=yyy"\n'
            '@ 1 IN TXT "low TTL for testing purpose"\n'
            '@ 1 IN TXT "MS=123"\n'
            '@ 1 IN TXT "v=spf1 ip6:2001:db8::2 ip4:192.0.2.2 ~all"\n'
            '@ 42 IN CAA 0 issue "letsencrypt.org"\n'
            '@ 42 IN CAA 0 issuewild ";"\n'
            '@ 42 IN CAA 0 iodef "mailto:security@example.com"\n'
            '@ 42 IN CAA 0 iodef "https://security.example.com/"\n'
            '@ 42 IN NAPTR 100 10 "s" "x-eduroam:radius.tls" "" _radsec._tcp.eduroam.de.\n'
            '@ 42 IN MX 10 mail\n'
            '@ 42 IN MX 100 mail.example.org.\n'
            '@ 42 IN NS ns-1\n'
            '@ 42 IN NS ns-2.example.org.\n'
            '@ 42 IN SOA ns-1 hostmaster 1 1 1 1 1\n'
            '@ 42 IN AAAA 2001:db8::1\n'
            '@ 42 IN A 192.0.2.1\n'
            '_xmpp-client._tcp 42 IN SRV 5 0 5222 xmpp-1.example.net.\n'
            '_xmpp-server._tcp 42 IN SRV 5 0 5269 xmpp-1.example.net.\n'
            'departement 42 IN DS 52037 1 1 378929e92d7da04267ee87e802d75c5ca1b5d280\n'
            'departement 42 IN NS ad.departement\n'
            'ad.departement 42 IN AAAA 2001:db8::3\n'
            'mail 42 IN AAAA 2001:db8::4\n'
            'mail 42 IN A 192.0.2.4\n'
            'ns-1 42 IN AAAA 2001:db8::\n'
            'ns-1 42 IN A 192.0.2.0\n'
            'staging 42 IN AAAA 2001:db8::1\n'
            'staging 42 IN A 192.0.2.1\n'
            'web-1 42 IN AAAA 2001:db8::2\n'
            'web-1 42 IN A 192.0.2.2\n'
            'web-1 42 IN LOC 34 15 28.000 S 72 8 0.000 W -5.00m\n'
            'web-1 42 IN SSHFP 2 1 123456789abcdef67890123456789abcdef67890\n'
            'www 42 IN AAAA 2001:db8::1\n'
            'www 42 IN A 192.0.2.1\n'
            'www2 42 IN CNAME @\n')

        reader = zonem.dns.zone.ZoneReader(ZONES_VALID.joinpath('example.com'), dns.name.from_text('example.com'), 42)
        reader.read()
        self.assertTrue(reader.zone.to_text().decode('ASCII') == zone_data)

    def test_read_invalid_symlink(self):
        """Do not cross base path."""

        zonem.utils.singleton.reset()

        reader = zonem.dns.zone.ZoneReader(
            ZONES_INVALID_SYMLINK.joinpath('example.com'), dns.name.from_text('example.com'), 42,
            ZONES_INVALID_SYMLINK.joinpath('example.com'))
        with self.assertRaises(ValueError):
            reader.read()

    def test_read_invalid_host(self):
        """Do not allow duplacte host entries."""

        zonem.utils.singleton.reset()

        reader = zonem.dns.zone.ZoneReader(
            ZONES_INVALID_HOST.joinpath('example.com'), dns.name.from_text('example.com'), 42,
            ZONES_INVALID_HOST.joinpath('example.com'))
        with self.assertRaises(ValueError, msg='Can not add host host2.example.com., \
because host host1.example.com. is already conected with address 2001:db8::2.'):
            reader.read()


class ZoneListTestCase(unittest.TestCase):
    """test zonem.dns.zone.ZoneList"""

    def test_zone_list(self):
        """test ZoneList"""
        zone_list = zonem.dns.zone.ZoneList()
        zone = dns.zone.Zone('example.com.')
        zone_list.append(zone)
        self.assertEqual(zone_list['example.com.'], zone)

    def test_write(self):
        """Write ZoneList to disk"""

        _ = self

        zonem.utils.singleton.reset()

        config = zonem.config.config.Config()
        config.base_dir = ZONES_VALID
        zone_list = zonem.dns.zone.collect_zones(ZONES_VALID, config)

        ptr_generator = zonem.post_processing.ptr.PTRGenerator(zone_list)
        ptr_generator.run()

        target_validator = zonem.post_processing.target.Validator(zone_list)
        target_validator.run(False)

        open_mock = unittest.mock.mock_open()
        with unittest.mock.patch("zonem.dns.zone.open", open_mock, create=True):
            zone_list.write('/tmp/zones/')
        calls = [
            unittest.mock.call('/tmp/zones/2.0.192.in-addr.arpa', 'wb'),
            unittest.mock.call().__enter__(),
            unittest.mock.call().write(b'@ 86400 IN SOA ns-1.example.com. hostmaster.example.com. 1 1 1 1 1\n\
0 86400 IN PTR ns-1.example.com.\n2 86400 IN PTR web-1.example.com.\n4 86400 IN PTR mail.example.com.\n'),
            unittest.mock.call().__exit__(None, None, None),
            unittest.mock.call('/tmp/zones/8.b.d.0.1.0.0.2.ip6.arpa', 'wb'),
            unittest.mock.call().__enter__(),
            unittest.mock.call().write(b'@ 86400 IN SOA ns-1.example.com. hostmaster.example.com. 1 1 1 1 1\n\
0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0 86400 IN PTR ns-1.example.com.\n\
2.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0 86400 IN PTR web-1.example.com.\n\
4.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0 86400 IN PTR mail.example.com.\n'),
            unittest.mock.call().__exit__(None, None, None),
            unittest.mock.call('/tmp/zones/example.com', 'wb'),
            unittest.mock.call().__enter__(),
            unittest.mock.call().write(b'@ 1 IN TXT "adobe-idp-site-verification=xyz"\n@ 1 IN TXT "comment"\n\
@ 1 IN TXT "google-site-verification=xxx"\n@ 1 IN TXT "google-site-verification=yyy"\n\
@ 1 IN TXT "low TTL for testing purpose"\n@ 1 IN TXT "MS=123"\n\
@ 1 IN TXT "v=spf1 ip6:2001:db8::2 ip4:192.0.2.2 ~all"\n\
@ 86400 IN CAA 0 issue "letsencrypt.org"\n@ 86400 IN CAA 0 issuewild ";"\n\
@ 86400 IN CAA 0 iodef "mailto:security@example.com"\n\
@ 86400 IN CAA 0 iodef "https://security.example.com/"\n\
@ 86400 IN NAPTR 100 10 "s" "x-eduroam:radius.tls" "" _radsec._tcp.eduroam.de.\n\
@ 86400 IN MX 10 mail\n@ 86400 IN MX 100 mail.example.org.\n@ 86400 IN NS ns-1\n\
@ 86400 IN NS ns-2.example.org.\n@ 86400 IN SOA ns-1 hostmaster 1 1 1 1 1\n\
@ 86400 IN AAAA 2001:db8::1\n@ 86400 IN A 192.0.2.1\n\
_xmpp-client._tcp 86400 IN SRV 5 0 5222 xmpp-1.example.net.\n\
_xmpp-server._tcp 86400 IN SRV 5 0 5269 xmpp-1.example.net.\n\
departement 86400 IN DS 52037 1 1 378929e92d7da04267ee87e802d75c5ca1b5d280\n\
departement 86400 IN NS ad.departement\nad.departement 86400 IN AAAA 2001:db8::3\n\
mail 86400 IN AAAA 2001:db8::4\nmail 86400 IN A 192.0.2.4\nns-1 86400 IN AAAA 2001:db8::\n\
ns-1 86400 IN A 192.0.2.0\nstaging 86400 IN AAAA 2001:db8::1\nstaging 86400 IN A 192.0.2.1\n\
web-1 86400 IN AAAA 2001:db8::2\nweb-1 86400 IN A 192.0.2.2\n\
web-1 86400 IN LOC 34 15 28.000 S 72 8 0.000 W -5.00m\n\
web-1 86400 IN SSHFP 2 1 123456789abcdef67890123456789abcdef67890\n\
www 86400 IN AAAA 2001:db8::1\nwww 86400 IN A 192.0.2.1\nwww2 86400 IN CNAME @\n'),
            unittest.mock.call().__exit__(None, None, None)]

        open_mock.assert_has_calls(calls)


class CollectZonesTestCase(unittest.TestCase):
    """test zonem.dns.zone.collect_zones"""

    def test_collect_zones(self):
        """test zonem.dns.zone.collect_zones"""

        zonem.utils.singleton.reset()

        config = zonem.config.config.Config()
        config.base_dir = ZONES_VALID
        zone_list = zonem.dns.zone.collect_zones(ZONES_VALID, config)
        self.assertIsInstance(zone_list, zonem.dns.zone.ZoneList)


class DiffTestCase(unittest.TestCase):
    """Test zonem.dns.zone.diff"""

    def test_diff(self):
        """Test zonem.dns.zone.diff"""

        zone1_data = (
            '$ORIGIN example.com.\n'
            '@ 42 IN NS ns-2.example.org.\n'
            '@ 42 IN SOA ns-2.example.org. hostmaster 1 1 1 1 1\n'
            '@ 42 IN AAAA 2001:db8::1\n'
            '@ 42 IN A 192.0.2.1\n'
            'www 42 IN CNAME @\n')
        zone1 = dns.zone.from_text(zone1_data)
        zone2_data = (
            '$ORIGIN example.com.\n'
            '@ 42 IN NS ns-2.example.org.\n'
            '@ 42 IN SOA ns-2.example.org. hostmaster 2 1 1 1 2\n'
            '@ 42 IN AAAA 2001:db8::1\n'
            '@ 42 IN A 192.0.2.2\n'
            'www 44 IN CNAME @\n')
        zone2 = dns.zone.from_text(zone2_data)
        missing_zone1, missing_zone2 = zonem.dns.zone.diff(zone1, zone2)
        self.assertEqual(
            missing_zone1.to_text(),
            b'@ 42 IN A 192.0.2.2\n@ 42 IN SOA ns-2.example.org. hostmaster 2 1 1 1 2\nwww 44 IN CNAME @\n')
        self.assertEqual(
            missing_zone2.to_text(),
            b'@ 42 IN A 192.0.2.1\n@ 42 IN SOA ns-2.example.org. hostmaster 1 1 1 1 1\nwww 42 IN CNAME @\n')
