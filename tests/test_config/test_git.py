"""DNS Zone"""

import pathlib
import unittest

import dns.name

import zonem.config
import zonem.utils

BASE_PATH = pathlib.Path(__file__).parent


class GITTestCase(unittest.TestCase):
    """test zonem.config.git.GIT"""

    def test_init(self):
        """Create an instance with default values"""

        zonem.utils.singleton.reset()

        git = zonem.config.git.GIT(False)
        cfile = BASE_PATH.joinpath('gitconfig')
        sfile = BASE_PATH.joinpath('secrets.yaml')
        git.load(cfile, sfile)
        self.assertIsInstance(git.config_dict, dict)
        data = {
            'config': {
                'default_ttl': '600',
                'exporter': ['ns-1.example.com'],
                'validate_external': False},
            'zones': {'example.com.': {'exporter': ['ns-2.example.com', 'ns-1.example.com'], 'unique_host': False}, },
            'exporter': {'ns-1.example.com': {'plugin': 'nsupdate', 'key': 'foobar'},
                         'ns-2.example.com': {'plugin': 'nsupdate'}}}
        self.assertEqual(git.config_dict, data)
        config = git.config
        self.assertIsInstance(config, zonem.config.config.Config)
        self.assertEqual(config[dns.name.from_text('example.com')].exporter[0].name, 'ns-2.example.com')
        self.assertEqual(config[dns.name.from_text('example.com')].exporter[1].name, 'ns-1.example.com')
        self.assertEqual(config[dns.name.from_text('8.b.d.0.1.0.0.2.ip6.arpa')].exporter[0].name, 'ns-1.example.com')
        self.assertEqual(config[dns.name.from_text('example.com')].unique_host, False)
        self.assertEqual(config[dns.name.from_text('8.b.d.0.1.0.0.2.ip6.arpa')].unique_host, True)
        self.assertEqual(config.validate_external, False)
