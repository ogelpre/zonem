"""DNS Zone"""

import unittest

import dns.name
import zonem.config
import zonem.utils


class TTLTestCase(unittest.TestCase):
    """test zonem.config.config.TTL"""

    def test_init(self):
        """Create an instance with default values"""
        ttl = zonem.config.config.TTL()
        self.assertEqual(int(ttl), 86400)

    def test_values(self):
        """Test TTL values."""
        ttl = zonem.config.config.TTL(0)
        self.assertEqual(int(ttl), 0)
        ttl = zonem.config.config.TTL(4294967295)
        self.assertEqual(ttl.value, 4294967295)
        with self.assertRaises(ValueError):
            ttl = zonem.config.config.TTL(4294967296)
        with self.assertRaises(ValueError):
            ttl = zonem.config.config.TTL(-1)


class ConfigTestCase(unittest.TestCase):
    """test zonem.config.Config"""

    def test_init(self):
        """Create an instance with default values"""

        zonem.utils.singleton.reset()

        config = zonem.config.config.Config()
        self.assertEqual(int(config.default_ttl), zonem.config.config.TTL.DEFAULT)
        self.assertEqual(config.serial, zonem.config.config.AUTO)
        self.assertIsInstance(config.zones, zonem.config.config.ConfigZoneList)
        # test __getitem__
        self.assertEqual(int(config[dns.name.from_text('test.')].default_ttl), zonem.config.config.TTL.DEFAULT)

    def test_singleton(self):
        """Test singleton"""

        zonem.utils.singleton.reset()

        config1 = zonem.config.config.Config()
        config2 = zonem.config.config.Config()
        self.assertEqual(config1, config2)
        config1.default_ttl = 42
        self.assertEqual(config1.default_ttl, config2.default_ttl)

    def test_setters(self):
        """Test setters"""

        zonem.utils.singleton.reset()

        config = zonem.config.config.Config()
        config.exporters.add(zonem.exporter.get('nsupdate')('ns-1'))
        config.exporter = 'ns-1'
        self.assertIsInstance(config.exporter[0], zonem.exporter.nsupdate.Exporter)
        config.default_ttl = 1
        self.assertEqual(int(config.default_ttl), 1)
        config.serial = zonem.config.config.DATE
        self.assertEqual(config.serial, zonem.config.config.DATE)

    def test_serial(self):
        """Test serial methods."""

        zonem.utils.singleton.reset()

        config = zonem.config.config.Config()
        self.assertEqual(config.serial, zonem.config.config.AUTO)
        config.serial = 'increment'
        self.assertEqual(config.serial, zonem.config.config.INCREMENT)
        config.serial = 'timestamp'
        self.assertEqual(config.serial, zonem.config.config.TIMESTAMP)
        config.serial = 'date'
        self.assertEqual(config.serial, zonem.config.config.DATE)
        with self.assertRaises(ValueError):
            config.serial = 'X'


class ConfigExporterList(unittest.TestCase):
    """test zonem.config.config.ConfigExporterList"""

    def test_init(self):
        """Create an instance with default values."""
        exporters = zonem.config.config.ConfigExporterList()
        exporter = zonem.exporter.base.BaseExporter('test')
        exporters.add(exporter)
        self.assertEqual(exporters['test'], exporter)


class ConfigZoneTestCase(unittest.TestCase):
    """test zonem.config.config.ConfigZone"""

    def test_init(self):
        """Create an instance with default values."""

        zonem.utils.singleton.reset()

        config_zone = zonem.config.config.ConfigZone(dns.name.from_text('example.com'))
        self.assertEqual(config_zone.origin, dns.name.from_text('example.com'))
        self.assertEqual(config_zone.default_ttl.value, zonem.config.config.TTL.DEFAULT)

    def test_ttl(self):
        """Test ttl value."""

        zonem.utils.singleton.reset()

        config_zone = zonem.config.config.ConfigZone(dns.name.from_text('example.com'), None, 1)
        self.assertEqual(config_zone.default_ttl.value, 1)
        config_zone.default_ttl = 2
        self.assertEqual(config_zone.default_ttl.value, 2)
        config_zone.default_ttl = None
        _ = zonem.config.config.Config().default_ttl = 3
        self.assertEqual(config_zone.default_ttl.value, 3)

    def test_exporter(self):
        """Test Exporter value."""

        config_zone = zonem.config.config.ConfigZone(dns.name.from_text('example.com'), None, 1)
        self.assertEqual(config_zone.exporter, [])


class ConfigZoneListTestCase(unittest.TestCase):
    """test zonem.config.config.TTL"""

    def test_init(self):
        """Create an instance with default values"""

        config_zone_list = zonem.config.config.ConfigZoneList()
        with self.assertRaises(KeyError):
            _ = config_zone_list['example.com']

    def test_add_zone_config(self):
        """Add a zone config"""

        config_zone_list = zonem.config.config.ConfigZoneList()
        config_zone = zonem.config.config.ConfigZone(dns.name.from_text('example.com'), '')
        config_zone_list.add(config_zone)
        self.assertEqual(config_zone_list[dns.name.from_text('example.com')], config_zone)
        self.assertEqual(config_zone_list[dns.name.from_text('example.com')].exporter, [])
