"""DNS Zone"""

import unittest

import zonem.exporter


class NSupdateExporterTestCase(unittest.TestCase):
    """test zonem.exporter.nsupdate.Exporter"""

    def test_init(self):
        """Create an instance with default values"""

        exporter = zonem.exporter.nsupdate.Exporter('test')
        self.assertEqual(exporter.name, 'test')
