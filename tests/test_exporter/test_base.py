"""DNS Zone"""

import unittest

import zonem.exporter


class BaseExporterTestCase(unittest.TestCase):
    """test zonem.exporter.base.BaseExporter"""

    def test_init(self):
        """Create an instance with default values"""

        exporter = zonem.exporter.base.BaseExporter('test')
        self.assertEqual(exporter.name, 'test')
