"""DNS Zone"""

import unittest

import zonem.exporter


class ExporterTestCase(unittest.TestCase):
    """test zonem.exporter"""

    def test_get(self):
        """Get exporter by name"""

        exporter_class = zonem.exporter.get('nsupdate')
        self.assertEqual(exporter_class, zonem.exporter.nsupdate.Exporter)
