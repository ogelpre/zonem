"""Allow direct calling of tests with -m flag."""

import pathlib
import sys
import unittest


def main():
    """Main function to run tests."""
    suites = unittest.defaultTestLoader.discover(pathlib.Path(__file__).parent)
    if not unittest.TextTestRunner(verbosity=2).run(suites).wasSuccessful():
        sys.exit(1)


if __name__ == "__main__":
    main()
