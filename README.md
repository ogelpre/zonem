# zone manager - extensible zone file management with git

Zone manager is a flexible tool to manage your zone files with git.

## History

Zone manager was written, because exisiting Tools didn't fit requirments.
Especalliy the management of PTR is sometimes difficult.

## Development

Here should be some text written which describes the development of zone manager.
The Maximum line length is 128 Characters, because the Monitors have enough space today.

### Prerequisites

Zone manager needs the following python libs:

* click
* click-log
* dnspython
* pyyaml

Zone manager needs the following additional software:

* git

During development it the following tools are needed:

* GNU make
* Flake8
* pydocstyle
* Pylint
* markdown2

### Workflow

TBD

### Run sources directly

In a production environment setup.py builds an executable.
To run zone manager directly from source you can run `python -m zonem` in the top directory of the sources.


### Clean working directory

To clean your working directory run `make clean`.
