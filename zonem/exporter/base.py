"""Exporter base class."""


class InvalidOption(Exception):
    """Error if an invald option is passed."""


class BaseExporter:
    """Exporter base class."""

    NAME = 'base'

    def __init__(self, name, **kwargs):
        if kwargs:
            exporter_name = self.__class__.NAME
            options = ",".join(kwargs.keys())
            raise InvalidOption("Options {} not supported by exporter {}".format(options, exporter_name))

        assert isinstance(name, str)
        self._name = name

    @property
    def name(self):
        """return name"""

        return self._name

    def export(self, zone):
        """Export zone"""

        raise NotImplementedError
