# pylint: disable=R0912,R0913,R0914,W0212,R0915
"""nsupdate exporter"""

import base64
import logging
import os

import dns.query
import dns.update
import dns.rcode
import dns.tsig
import dns.zone

from zonem.exporter import base
import zonem.dns


class Exporter(base.BaseExporter):
    """nsupdate exporter"""

    NAME = 'nsupdate'

    def __init__(self, name, server=None, port=None, key=None, keyname=None, timeout=None, retries=None, source=None, **kwargs):
        super().__init__(name, **kwargs)

        if server is None:
            self.server = '::1'
        else:
            self.server = server
        if port is None:
            self.port = 53
        else:
            self.port = int(port)
        self.key = key
        self.keyname = keyname
        if timeout is None:
            self.timeout = 5
        else:
            self.timeout = int(timeout)
        if retries is None:
            self.retries = 1
        else:
            self.retries = int(retries)
        self.source = source

    def export(self, zone):
        # timeout=self.timeout
        if self.keyname is None:
            keyname = zone.origin
        else:
            keyname = dns.name.from_text(self.keyname)
        if self.key is None:
            keyring = None
        else:
            keyring = {keyname: base64.b64decode(self.key.encode())}
        keyalgorithm = dns.tsig.HMAC_SHA512
        query = dns.query.xfr(
            self.server, zone.origin, port=self.port, source=self.source,
            keyring=keyring, keyname=keyname, keyalgorithm=keyalgorithm)
        retries = self.retries
        try:
            while True:
                try:
                    old_zone = dns.zone.from_xfr(query, check_origin=False)
                    break
                except ConnectionRefusedError as error:
                    retries -= 1
                    if retries < 1:
                        raise Exception(error) from error
        except Exception as error:
            raise Exception("Exporter {} failed: {}".format(self.name, error)) from error
        rdatasets_add, rdatasets_remove = zonem.dns.zone.diff(old_zone, zone)
        for record in rdatasets_remove.to_text().split(b'\n'):
            logging.info("Zone {zone} remove: {rr}", zone=zone.origin, rr=record)
        for record in rdatasets_add.to_text().split(b'\n'):
            logging.info("Zone {zone} add: {r}", zone=zone.origin, rr=record)
        update_message = dns.update.Update(zone.origin, keyring=keyring, keyname=keyname, keyalgorithm=keyalgorithm)
        for label, rdatasets in rdatasets_remove.nodes.items():
            for rdataset in rdatasets:
                if rdataset.rdtype == dns.rdatatype.SOA:
                    continue
                update_message.delete(label, rdataset)
        for label in rdatasets_add.keys():
            node = rdatasets_add.get_node(label)
            for rdataset in node.rdatasets:
                if rdataset.rdtype == dns.rdatatype.SOA:
                    old_serial = old_zone.get_rdataset(zone.origin, dns.rdatatype.SOA)[-1].serial
                    rdataset[-1].serial = old_serial + 1
                update_message.add(label, rdataset)
        retries = self.retries
        try:
            while True:
                if not update_message.authority:
                    response = None
                    break
                try:
                    # timeout
                    response = dns.query.tcp(update_message, self.server, port=self.port, source=self.source)
                    break
                except ConnectionRefusedError as error:
                    retries -= 1
                    if retries < 1:
                        raise Exception(error) from error
        except Exception as error:
            raise Exception("Exporter {} failed: {}".format(self.name, error)) from error
        if response is not None and response.rcode() != dns.rcode.NOERROR:
            error = "Export of zone {} via Exporter {} failed.{}".format(zone.origin, self.NAME, os.linesep)
            error += str(response)
            raise RuntimeError(error)
