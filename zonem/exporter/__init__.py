"""export updates"""
import pkgutil
__all__ = []
for loader, name, is_pkg in pkgutil.iter_modules(__path__):
    if name == 'base':
        continue
    globals()[name] = loader.find_module(name).load_module(name)
    __all__.append(name)


def get(exporter_name):
    """Get exporter by name."""
    if exporter_name not in __all__:
        raise KeyError("Exporter {} not installed.".format(exporter_name))
    return globals()[exporter_name].Exporter
