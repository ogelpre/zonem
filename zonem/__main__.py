# disable false positive warning
# pylint: disable=E1120
"""Allow direct calling with -m flag."""

from zonem.cli import cli


def main():
    """Main function to run zone manager."""
    cli()


if __name__ == "__main__":
    main()
