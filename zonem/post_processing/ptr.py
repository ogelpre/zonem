"""Create PTR for hosts"""

import ipaddress

import dns.rdataclass
import dns.rdtypes.ANY.PTR
import dns.reversename

import zonem.utils


class HostList(zonem.utils.singleton.Singleton):
    """List of hosts for PTR generation"""

    def __init__(self):
        self._hosts = {}

    def __getitem__(self, address):
        return self._hosts[address]

    def reset(self):
        self._hosts = {}

    def items(self):
        """Return hosts as itterable."""
        return self._hosts.items()

    def add(self, address, host, ttl, unique_host=True):
        """Add host to host list"""
        address = str(ipaddress.ip_address(address))
        if address in self._hosts and unique_host:
            raise ValueError("Can not add host {}, because host {} is already conected with address {}.".format(
                host, self._hosts[address], address))
        self._hosts[address] = (host, ttl)


class PTRGenerator:
    """Create PTR for hosts"""
    def __init__(self, zones):
        self._zones = zones
        self._hosts = HostList()

    def run(self):
        """Run ptr generator"""
        for address, (host, ttl) in self._hosts.items():
            ptr_name = dns.reversename.from_address(address)
            zone = self._zones.get(ptr_name)
            if zone is None:
                raise ValueError("PTR generation failed, because zone not found for {}.".format(ptr_name))
            ptr_name_relativized = ptr_name.relativize(zone.origin)
            node = zone.nodes.get(ptr_name_relativized)
            if node is None:
                node = zone.node_factory()
                zone.nodes[ptr_name_relativized] = node
            rdataclass = dns.rdataclass.IN
            rdatatype = dns.rdatatype.PTR
            rdata = dns.rdtypes.ANY.PTR.PTR(rdataclass, rdatatype, host)
            covers = rdata.covers()
            rdataset = node.find_rdataset(rdataclass, rdatatype, covers, True)
            rdataset.add(rdata, ttl)
