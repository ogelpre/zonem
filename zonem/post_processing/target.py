# pylint: disable=R0912,R0914,R1702
"""Validate targets in records

Targets in CNAME, DNAME, MX and SRV records are checked for existence.
"""

import dns.rdataclass
import dns.rdatatype
import dns.resolver

import zonem.utils

RDTYPES = [
    dns.rdatatype.CNAME,
    dns.rdatatype.DNAME,
    dns.rdatatype.MX,
    dns.rdatatype.SRV]


class NameSet(zonem.utils.singleton.Singleton):
    """List of names of records with targets."""

    def __init__(self):
        self._names = set()

    def reset(self):
        self._names = set()

    def __iter__(self):
        """Return names set as itterable."""
        return iter(self._names)

    def add(self, name):
        """Add name to set"""
        self._names.add(name)


class Validator:
    """Validator for records with targets"""
    def __init__(self, zones):
        self._zones = zones
        self._name_set = NameSet()

    def run(self, validate_external=True):
        """Validate records with targets"""
        for name in self._name_set:
            zone = self._zones.get(name)
            if zone is None:
                raise RuntimeError("Zone for {} not found.".format(name.to_text()))
            node = zone.get_node(name)
            xname_rdatasets = []
            with_target_rdatasets = []
            rest_rdatasets = []
            for rdataset in node.rdatasets:
                if rdataset.rdtype in [dns.rdatatype.CNAME, dns.rdatatype.DNAME]:
                    xname_rdatasets.append(rdataset)
                elif rdataset.rdtype in [dns.rdatatype.MX, dns.rdatatype.SRV]:
                    with_target_rdatasets.append(rdataset)
                else:
                    rest_rdatasets.append(rdataset)
            if len(xname_rdatasets) > 1:
                cname_check_passed = False
            elif len(xname_rdatasets) == 1 and \
                    (len(with_target_rdatasets) + len(rest_rdatasets) > 0 or
                     len(xname_rdatasets[-1]) > 1):
                cname_check_passed = False
            else:
                cname_check_passed = True
            if not cname_check_passed:
                raise RuntimeError("No other records are allowed beside CNAME or DNAME ({})".format(name.to_text()))
            for rdataset in xname_rdatasets + with_target_rdatasets:
                rdtype = rdataset.rdtype
                for resource_record in rdataset:
                    if rdtype in [dns.rdatatype.CNAME, dns.rdatatype.DNAME, dns.rdatatype.SRV]:
                        target = resource_record.target.derelativize(zone.origin)
                    elif rdtype in [dns.rdatatype.MX]:
                        target = resource_record.exchange.derelativize(zone.origin)
                    target_zone = self._zones.get(target)
                    if target_zone is not None:
                        target_node = target_zone.get_node(target)
                        if target_node is None:
                            raise ValueError("Target {} for {} {} does not exist".format(
                                target.to_text(), dns.rdatatype.to_text(rdtype), name.to_text()))
                        for _rdtype in [dns.rdatatype.AAAA, dns.rdatatype.A, dns.rdatatype.CNAME, dns.rdatatype.DNAME]:
                            if target_node.get_rdataset(dns.rdataclass.IN, _rdtype):
                                break
                        else:
                            raise ValueError("Target {} for {} {} has no A, AAAA, CNAME or DNAME record".format(
                                target.to_text(), dns.rdatatype.to_text(rdtype), name.to_text()))
                    elif validate_external:
                        for rdatatype in [dns.rdatatype.AAAA, dns.rdatatype.A, dns.rdatatype.CNAME, dns.rdatatype.DNAME]:
                            try:
                                answer = dns.resolver.query(target, rdatatype).response.answer
                            except dns.resolver.NoAnswer:
                                continue
                            except dns.resolver.NXDOMAIN:
                                continue
                            if answer:
                                break
                        else:
                            raise ValueError("External target {} for {} {} does not exist".format(
                                target.to_text(), dns.rdatatype.to_text(rdtype), name.to_text()))
