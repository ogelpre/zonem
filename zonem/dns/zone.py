# pylint: disable=R0902,R0912,R0913,R0914,R0915
"""DNS Zone"""

import pathlib
import sys

import dns.name
import dns.rdataclass
import dns.rdatatype
import dns.tokenizer
import dns.zone

import zonem.post_processing


class ZoneReader:
    """Build zone by walking through directory hierachiy."""

    def __init__(self, base_dir, origin, default_ttl=None, limit_dir=None, unique_host=True):
        self._base_dir = pathlib.Path(base_dir)
        assert isinstance(origin, dns.name.Name)
        self._origin = origin
        self._default_ttl = default_ttl
        if limit_dir is None:
            self._limit_dir = None
        else:
            self._limit_dir = pathlib.Path(limit_dir).resolve()
        assert isinstance(unique_host, bool)
        self._unique_host = unique_host

        self._walker = ZoneWalker(self._base_dir, self._origin, self._limit_dir)
        self._zone = dns.zone.Zone(self._origin)
        self._hosts = zonem.post_processing.ptr.HostList()
        self._targets = zonem.post_processing.target.NameSet()

    @property
    def base_dir(self):
        """Return base_dir."""
        return self._base_dir

    @property
    def default_ttl(self):
        """Return default_ttl."""
        return self._default_ttl

    @property
    def origin(self):
        """Return origin."""
        return self._origin

    @property
    def zone(self):
        """Return zone."""
        return self._zone

    def _read_line(self, label, tokenizer, filename):
        """Read config line and insert it into zone."""

        token = tokenizer.get(True, True)

        if token.value.startswith('$'):
            raise dns.exception.SyntaxError('$ directives are not supported.')
        if token.is_whitespace():
            token = tokenizer.get()
            if token.is_eol_or_eof():
                return
        else:
            tokenizer.unget(token)

        label = label.relativize(self._origin)

        token = tokenizer.get()
        if not token.is_identifier():
            raise dns.exception.SyntaxError

        try:
            ttl = dns.ttl.from_text(token.value)
            token = tokenizer.get()
            if not token.is_identifier():
                raise dns.exception.SyntaxError
        except dns.ttl.BadTTL:
            ttl = self._default_ttl

        rdataclass = dns.rdataclass.IN

        try:
            rdatatype = dns.rdatatype.from_text(token.value)
        except Exception:
            raise dns.exception.SyntaxError("unknown rdatatype '{}'".format(token.value))

        node = self._zone.nodes.get(label)
        if node is None:
            node = self._zone.node_factory()
            self._zone.nodes[label] = node
        try:
            rdata = dns.rdata.from_text(rdataclass, rdatatype, tokenizer, self._origin, True)
        except dns.exception.SyntaxError:
            raise
        except Exception:
            traceback_type, traceback_value = sys.exc_info()[2]
            raise dns.exception.SyntaxError("caugh exception {}: {}".format(str(traceback_type), str(traceback_value)))

        if filename.name == 'host' or filename.name.endswith('.host'):
            if rdatatype not in [dns.rdatatype.AAAA, dns.rdatatype.A]:
                raise ValueError("Host config files can only contain AAAA and A records. Found: {}".format(
                    dns.rdatatype.to_text(rdatatype)))
            host_label = label.derelativize(self._zone.origin)
            self._hosts.add(rdata, host_label, ttl, self._unique_host)

        if rdatatype in zonem.post_processing.target.RDTYPES:
            self._targets.add(label.derelativize(self._zone.origin))

        covers = rdata.covers()
        rdataset = node.find_rdataset(rdataclass, rdatatype, covers, True)
        rdataset.add(rdata, ttl)

    def read(self):
        """Read zone data recursively from base_dir to create zone object."""
        for label, data, filename in self._walker.walk():
            tokenizer = dns.tokenizer.Tokenizer(data, filename)
            while True:
                token = tokenizer.get(True, True)
                if token.is_eof():
                    break
                if token.is_eol():
                    continue
                if token.is_comment():
                    token.get_eol()
                    continue
                tokenizer.unget(token)
                try:
                    self._read_line(label, tokenizer, filename)
                except dns.exception.SyntaxError as error_message:
                    (filename, line_number) = tokenizer.where()
                    if error_message is None:
                        error_message = "syntax error"
                    exception = dns.exception.SyntaxError("{}:{}: {}".format(filename, line_number, error_message))
                    traceback = sys.exc_info()[2]
                    raise exception.with_traceback(traceback) from None


class ZoneWalker:
    """Walk zone tree and return data."""

    def __init__(self, base_dir, origin, limit_dir=None):
        self._base_dir = pathlib.Path(base_dir)
        assert isinstance(origin, dns.name.Name)
        self._origin = origin
        if limit_dir is None:
            self._limit_dir = None
        else:
            self._limit_dir = pathlib.Path(limit_dir)

    @property
    def base_dir(self):
        """Return base_dir"""
        return self._base_dir

    @property
    def origin(self):
        """Return origin"""
        return self._origin

    def walk(self, path=None, parent_label=None):
        """Walk base_dir and return data."""
        if path is None:
            path = self.base_dir
        if parent_label is None:
            parent_label = self._origin
        for _path in sorted(path.iterdir()):
            original_label = None
            label = None
            if _path.is_symlink():
                original_label = dns.name.Name([_path.name]) + parent_label
                _path = pathlib.Path(_path.resolve())
                if self._limit_dir is not None and self._limit_dir not in _path.parents:
                    raise ValueError('Destination of {} is not inside of {}.'.format(_path, self._limit_dir))
            if _path.is_dir():
                if original_label is None:
                    label = dns.name.Name([_path.name]) + parent_label
                else:
                    label = original_label
                for label, data, filename in self.walk(_path, label):
                    yield label, data, filename
            if _path.is_file():
                label = parent_label
                filename = _path
                with open(filename) as stream:
                    data = stream.read()
                yield label, data, filename


class ZoneList:
    """List of Zones."""

    def __init__(self):
        self._zones = {}

    def __getitem__(self, origin):
        if isinstance(origin, str):
            origin = dns.name.from_text(origin)
        return self._zones[origin]

    def __iter__(self):
        return iter(self._zones.values())

    def append(self, zone):
        """Add zone to list."""
        assert isinstance(zone, dns.zone.Zone)
        self._zones[zone.origin] = zone

    def get(self, name):
        """Get zone by domainname"""
        if isinstance(name, str):
            name = dns.name.from_text(name)
        for origin, zone in self._zones.items():
            if name.is_subdomain(origin):
                node_name = name.parent()
                while node_name.is_subdomain(origin):
                    node = zone.nodes.get(node_name)
                    if node is not None:
                        rdataset = node.get_rdataset(dns.rdataclass.IN, dns.rdatatype.A)
                        if rdataset is not None:
                            break
                    node_name = node_name.parent()
                else:
                    return zone
        return None

    def write(self, output_dir, suffix=None):
        """Write zones to a directory."""
        for _, zone in sorted(self._zones.items()):
            if suffix is None:
                suffix = ''
            filename = zone.origin.to_text(omit_final_dot=not suffix) + suffix
            path = pathlib.Path(output_dir).joinpath(filename)
            if path.exists():
                raise RuntimeError("Zonefile {} exists".format(path))
            zone_data = zone.to_text()
            with open(str(path), 'wb') as stream:
                stream.write(zone_data)


def collect_zones(base_path, config):
    """Collect zones from base_path."""
    _base_path = pathlib.Path(base_path)
    if not _base_path.is_dir():
        raise ValueError('base_dir has to be adirectory')
    zone_paths = []
    for path in _base_path.iterdir():
        if path.name.startswith('.'):
            continue
        if path.name.endswith('.'):
            raise ValueError("Path should not end with colon: {}".format(path))
        if path.is_symlink():
            real_path = pathlib.Path(path.resolve())
        else:
            real_path = path
        if real_path.is_dir():
            if _base_path not in real_path.parents:
                raise ValueError("Destination of {} is not inside of {}.".format(path, _base_path))
            zone_paths.append(path)
    zone_list = ZoneList()
    for zone_path in zone_paths:
        origin = dns.name.from_text(zone_path.name)
        default_ttl = config[origin].default_ttl.value
        unique_host = config[origin].unique_host
        zone_reader = ZoneReader(zone_path, origin, default_ttl, base_path, unique_host)
        zone_reader.read()
        zone = zone_reader.zone
        zone_list.append(zone)
    return zone_list


def diff(zone1, zone2, ignore_serial=True, ignore_dnssec=True):
    """Diff two zones."""

    assert isinstance(zone1, dns.zone.Zone)
    assert isinstance(zone2, dns.zone.Zone)
    assert zone1.origin == zone2.origin

    missing_in_zone1 = dns.zone.Zone(zone1.origin)
    missing_in_zone2 = dns.zone.Zone(zone2.origin)

    labels = set(zone1.keys()) | set(zone2.keys())
    for label in labels:
        node1 = zone1.get_node(label)
        node2 = zone2.get_node(label)
        rdtypes = set()
        for node in (node1, node2):
            if node is not None:
                for rdataset in node.rdatasets:
                    if rdataset.rdtype == dns.rdatatype.SOA:
                        continue
                    rdtypes.add(rdataset.rdtype)
            if ignore_dnssec:
                ignore_rdtype = [
                    dns.rdatatype.CDNSKEY,
                    dns.rdatatype.CDS,
                    dns.rdatatype.DNSKEY,
                    dns.rdatatype.NSEC,
                    dns.rdatatype.NSEC3,
                    dns.rdatatype.NSEC3PARAM,
                    dns.rdatatype.RRSIG]
            else:
                ignore_rdtype = []
        for rdtype in rdtypes:
            if rdtype in ignore_rdtype:
                continue

            if node1 is None:
                rdataset1 = None
            else:
                rdataset1 = node1.get_rdataset(dns.rdataclass.IN, rdtype)
            if rdataset1 is None:
                rdataset1 = dns.rdataset.Rdataset(dns.rdataclass.IN, rdtype)
            if node2 is None:
                rdataset2 = None
            else:
                rdataset2 = node2.get_rdataset(dns.rdataclass.IN, rdtype)
            if rdataset2 is None:
                rdataset2 = dns.rdataset.Rdataset(dns.rdataclass.IN, rdtype)
            if rdataset1.ttl == rdataset2.ttl:
                rdataset_missing_in_zone1 = rdataset2 - rdataset1
                rdataset_missing_in_zone2 = rdataset1 - rdataset2
            else:
                rdataset_missing_in_zone1 = rdataset2
                rdataset_missing_in_zone2 = rdataset1
            if rdataset_missing_in_zone1:
                node = missing_in_zone1.find_node(label, True)
                covers = rdataset_missing_in_zone1.covers
                rdataset = node.find_rdataset(rdataset1.rdclass, rdataset1.rdtype, covers, True)
                rdataset.union_update(rdataset_missing_in_zone1)
            if rdataset_missing_in_zone2:
                node = missing_in_zone2.find_node(label, True)
                covers = rdataset_missing_in_zone2.covers
                rdataset = node.find_rdataset(rdataset1.rdclass, rdataset1.rdtype, covers, True)
                rdataset.union_update(rdataset_missing_in_zone2)
    rdataset1 = zone1.get_rdataset(zone1.origin, dns.rdatatype.SOA)
    rdataset2 = zone2.get_rdataset(zone2.origin, dns.rdatatype.SOA)
    if rdataset1 is None:
        ttl1 = None
        soa1 = None
    else:
        ttl1 = rdataset1.ttl
        soa1 = rdataset1[-1]
    if rdataset2 is None:
        ttl1 = None
        soa2 = None
    else:
        ttl2 = rdataset2.ttl
        soa2 = rdataset2[-1]
    if ignore_serial and soa1 is not None and soa2 is not None:
        _soa1 = dns.rdata.from_text(rdataset1.rdclass, rdataset1.rdtype, soa1.to_text())
        _soa2 = dns.rdata.from_text(rdataset2.rdclass, rdataset2.rdtype, soa2.to_text())
        _soa1.serial = 0
        _soa2.serial = 0
    else:
        _soa1 = soa1
        _soa2 = soa2
    if _soa1 != _soa2 or rdataset1.ttl != rdataset2.ttl:
        for _zone, _soa, _ttl in ((missing_in_zone1, soa2, ttl2), (missing_in_zone2, soa1, ttl1)):
            if _soa is None:
                continue
            node = _zone.find_node(zone1.origin, True)
            covers = _soa.covers()
            rdataset = node.find_rdataset(_soa.rdclass, _soa.rdtype, covers, True)
            rdataset.add(_soa, _ttl)

    return missing_in_zone1, missing_in_zone2
