# allow more instance attributes and arguments
# disable false positive for __new__
# pylint: disable=R0902,R0913,W0613
"""Provides the zonem config objects"""

import pathlib

import dns.name

import zonem.exporter
import zonem.utils

AUTO = 0
INCREMENT = 1
TIMESTAMP = 2
DATE = 3

SERIAL_MAPPING = {
    'auto': AUTO,
    'increment': INCREMENT,
    'timestamp': TIMESTAMP,
    'date': DATE}

SERIAL = AUTO
REFRESH = 86400
RETRY = 7200
EXPIRE = 3600


class TTL:
    """TTL type."""
    LOWER = 0
    UPPER = 2**32-1
    DEFAULT = 86400

    def __init__(self, value=None):
        if value is None:
            value = TTL.DEFAULT
        else:
            value = int(value)
        if isinstance(value, int) and TTL.LOWER <= value <= TTL.UPPER:
            self._value = value
        else:
            raise ValueError("TTL value must be between {} and {}, not {}".format(TTL.LOWER, TTL.UPPER, value))

    def __int__(self):
        """Return value as int."""
        return self._value

    @property
    def value(self):
        """Return value."""
        return self._value


class Config(zonem.utils.singleton.Singleton):
    """Zonem config object"""

    def __init__(self):
        self._base_dir = pathlib.Path().resolve()
        self._exporters = None
        self.exporters = None
        self._zones = ConfigZoneList()
        self.exporter = None
        self.default_ttl = None
        self.serial = None
        self.unique_host = True
        self._bare = False
        self._validate_external = True

    def __getitem__(self, origin):
        assert isinstance(origin, dns.name.Name)
        try:
            return self._zones[origin]
        except KeyError:
            return ConfigZone(origin)

    def reset(self):
        self.base_dir = pathlib.Path().resolve()
        self._exporters = None
        self.exporters = None
        self._zones = ConfigZoneList()
        self.exporter = None
        self.default_ttl = None
        self.serial = None
        self.unique_host = True
        self._bare = False
        self._validate_external = True

    @property
    def base_dir(self):
        """Return base path."""
        return self._base_dir

    @base_dir.setter
    def base_dir(self, value):
        """Set base_dir."""
        self._base_dir = pathlib.Path(value).resolve()

    @property
    def exporter(self):
        """Return exporter."""
        return self._exporter

    @exporter.setter
    def exporter(self, value):
        """Set exporter."""
        if value is None:
            self._exporter = []
            return
        if isinstance(value, str):
            if len(value) == 0:
                value = []
            else:
                value = [value]
        if isinstance(value, list):
            if self.exporters is None:
                raise RuntimeError('Exporters not loaded yet.')
            exporter_list = []
            for exporter in value:
                if len(exporter) == 0:
                    continue
                if isinstance(exporter, str):
                    exporter_list.append(self._exporters[exporter])
                else:
                    raise ValueError("Exporter {} is invalid".format(exporter))
            self._exporter = exporter_list
        else:
            raise ValueError("Exporter {} is invalid".format(exporter))

    @property
    def default_ttl(self):
        """Return default ttl."""
        return self._default_ttl

    @default_ttl.setter
    def default_ttl(self, value):
        """Set default ttl."""
        self._default_ttl = TTL(value)

    @property
    def serial(self):
        """Return serial method."""
        return self._serial

    @serial.setter
    def serial(self, value):
        """Set serial method."""
        if isinstance(value, int):
            if value not in (AUTO, INCREMENT, TIMESTAMP, DATE):
                raise ValueError("Serial method {} is not supported.".format(value))
        elif isinstance(value, str):
            try:
                value = SERIAL_MAPPING[value]
            except KeyError:
                raise ValueError("Serial method {} is not supported.".format(value))
        elif value is None:
            value = SERIAL
        else:
            raise ValueError("Unknown serial method {}.".format(value))
        self._serial = value

    @property
    def unique_host(self):
        """Return unique hsot setting."""
        return self._unique_host

    @unique_host.setter
    def unique_host(self, value):
        assert isinstance(value, bool)
        self._unique_host = value

    @property
    def zones(self):
        """Return zones config."""
        return self._zones

    @property
    def exporters(self):
        """Return exporter."""
        return self._exporters

    @exporters.setter
    def exporters(self, exporters):
        """Set exporter."""
        if self._exporters is not None:
            raise RuntimeError('Exporters is already configured.')
        if exporters is None:
            self._exporters = ConfigExporterList()
        elif isinstance(exporters, ConfigExporterList):
            self._exporters = exporters
        else:
            raise TypeError("ConfigExportList or None expected, got {}".format(type(exporters)))

    @property
    def bare(self):
        """Return bare setting."""
        return self._bare

    @bare.setter
    def bare(self, value):
        """Set bare setting."""
        self._bare = bool(value)

    @property
    def validate_external(self):
        """Return validate_external setting."""
        return self._validate_external

    @validate_external.setter
    def validate_external(self, value):
        """Set validate external setting."""
        self._validate_external = bool(value)


class ConfigZoneList:
    """List of zone configs."""

    def __init__(self):
        self._zones = {}

    def __getitem__(self, origin):
        return self._zones[origin]

    def add(self, config_zone):
        """Add zone config"""
        assert isinstance(config_zone, ConfigZone)
        assert config_zone.origin not in self._zones
        self._zones[config_zone.origin] = config_zone


class ConfigExporterList:
    """List of exporters."""

    def __init__(self):
        self._exporters = {}

    def __getitem__(self, exporter):
        return self._exporters[exporter]

    def add(self, exporter):
        """Add exporter"""
        assert isinstance(exporter, zonem.exporter.base.BaseExporter)
        assert exporter.name not in self._exporters
        self._exporters[exporter.name] = exporter


class ConfigZone:
    """Zone config."""
    def __init__(self, origin, exporter=None, default_ttl=None, unique_host=None):
        self.exporter = exporter
        self.default_ttl = default_ttl
        self._set_origin(origin)
        self.unique_host = unique_host

    @property
    def origin(self):
        """Return origin."""
        return self._origin

    def _set_origin(self, value):
        """Set origin."""
        assert isinstance(value, dns.name.Name)
        self._origin = value

    @property
    def exporter(self):
        """Return exporter."""
        if self._exporter is None:
            return Config().exporter
        return self._exporter

    @exporter.setter
    def exporter(self, value):
        """Set exporter."""
        if value is None:
            self._exporter = None
            return
        if isinstance(value, str):
            if len(value) == 0:
                value = []
            else:
                value = [value]
        if isinstance(value, list):
            if Config().exporters is None:
                raise RuntimeError('Exporters not loaded yet.')
            exporter_list = []
            for exporter in value:
                if isinstance(exporter, str):
                    if len(exporter) == 0:
                        continue
                    exporter_list.append(Config().exporters[exporter])
                else:
                    raise ValueError("Exporter {} is invalid".format(exporter))
            self._exporter = exporter_list
        else:
            raise ValueError("Exporter {} is invalid".format(value))

    @property
    def default_ttl(self):
        """Return default ttl."""
        if self._default_ttl is None:
            return Config().default_ttl
        return self._default_ttl

    @default_ttl.setter
    def default_ttl(self, value):
        """Set default ttl."""
        if value is None:
            self._default_ttl = None
        else:
            self._default_ttl = TTL(value)

    @property
    def unique_host(self):
        """Return unique hsot setting."""
        if self._unique_host is None:
            return Config().unique_host
        return self._unique_host

    @unique_host.setter
    def unique_host(self, value):
        if value is None:
            self._unique_host = None
        else:
            self._unique_host = bool(value)


def from_dict(config_dict):
    """Build config from dictionary."""
    config = Config()
    if 'bare' in config_dict:
        config.bare = config_dict['bare']
    for exporter_name, exporter_config in config_dict['exporter'].items():
        exporter_config = exporter_config.copy()
        try:
            plugin = exporter_config.pop('plugin')
        except KeyError:
            raise ValueError("Exporter {}: missing plugin option.".format(exporter_name))
        try:
            exporter_class = zonem.exporter.get(plugin)
        except KeyError as error:
            raise RuntimeError("Exporter {}: {}".format(exporter_name, error))
        config.exporters.add(exporter_class(name=exporter_name, **exporter_config))
    for option, value in config_dict['config'].items():
        if hasattr(config, option):
            setattr(config, option, value)
        else:
            raise ValueError("Global config option {} not supported.".format(option))
    for origin, zone_config in config_dict['zones'].items():
        origin = dns.name.from_text(origin)
        config_zone = ConfigZone(origin, **zone_config)
        config.zones.add(config_zone)
    return config
