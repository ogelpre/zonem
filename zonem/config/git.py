"""git config handlers"""

import subprocess

import dns.name
import yaml

import zonem.config


def clean_value(value, option):
    """clean git config values"""
    if value == 'true':
        value = True
    if value == 'false':
        value = False
    if option == 'exporter':
        value = value.split(',')
    return value


class GIT:
    """GIT config  interface"""
    def __init__(self, bare_check=True):
        self._config_dict = {}
        self._config_dict['config'] = {}
        self._config_dict['zones'] = {}
        self._config_dict['exporter'] = {}
        if bare_check and self._is_bare():
            self._config_dict['bare'] = True
        self._config = None

    @property
    def config_dict(self):
        """Return config dictionary."""
        return self._config_dict

    @property
    def config(self):
        """Return config."""
        if self._config is None:
            self._build_config()
        return self._config

    @staticmethod
    def _is_bare():
        """Check if git is bare."""
        cmd = ['git', 'rev-parse', '--is-bare-repository']
        stdout = run_git(cmd).strip()
        if stdout == 'true':
            return True
        if stdout == 'false':
            return False
        raise RuntimeError('Can not determine if bare repository or not.')

    def load(self, config_file=None, secrets_file=None):
        """load all configs from git"""

        if secrets_file is None:
            secrets = {}
        else:
            with open(secrets_file) as stream:
                secrets = yaml.safe_load(stream)

        cmd = ['git', 'config', '--local', '--list', '--includes']
        if config_file:
            cmd.remove('--local')
            cmd.extend(['--file', config_file])
        stdout = run_git(cmd)
        for line in stdout.split():
            if not line.startswith('zonem'):
                continue
            section, rest = line.split('.', 1)
            if section == 'zonem':
                option, value = rest.split('=', 1)
                option = option.replace('-', '_')
                value = value.format(**secrets)
                self.config_dict['config'][option] = clean_value(value, option)
            elif section == 'zonem-zone':
                rest, value = rest.split('=', 1)
                origin, option = rest.rsplit('.', 1)
                origin = dns.name.from_text(origin).to_text()
                option = option.replace('-', '_')
                value = value.format(**secrets)
                if origin not in self.config_dict['zones']:
                    self.config_dict['zones'][origin] = {}
                self.config_dict['zones'][origin][option] = clean_value(value, option)
            elif section == 'zonem-exporter':
                rest, value = rest.split('=', 1)
                exporter, option = rest.rsplit('.', 1)
                option = option.replace('-', '_')
                value = value.format(**secrets)
                if exporter not in self.config_dict['exporter']:
                    self.config_dict['exporter'][exporter] = {}
                self.config_dict['exporter'][exporter][option] = clean_value(value, option)

    def _build_config(self):
        """Build config."""
        self._config = zonem.config.config.from_dict(self._config_dict)


def run_git(cmd, env=None):
    """Wrapper to run git command."""
    try:
        proc = subprocess.run(cmd, capture_output=True, check=True, env=env)
    except subprocess.CalledProcessError as error:
        if error.stderr.decode('utf-8') == 'fatal: --local can only be used inside a git repository' or \
                error.stderr.decode('utf-8').startswith('fatal: not a git repository'):
            raise RuntimeError('No git repository found.')
        raise RuntimeError("There was an error getting data from git: {}".format(error.stderr.decode('utf-8')))
    except FileNotFoundError:
        raise RuntimeError('Can not find git binary.')
    except Exception as error:
        raise RuntimeError("Running git binary failed: {}".format(error))
    stdout = proc.stdout.decode('utf-8')
    return stdout
