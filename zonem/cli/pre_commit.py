"""pre-commit hook for git"""

import click

import zonem.main


@click.command(short_help='pre-commit hook for git')
@click.pass_context
def cli(ctx):
    """pre-commit hook"""

    main = zonem.main.Main(**ctx.obj)
    try:
        main.lint()
    finally:
        main.cleanup()
