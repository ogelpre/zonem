"""lint zones"""

import click

import zonem.main


@click.command(short_help='lint zones')
@click.pass_context
def cli(ctx):
    """lint zones"""

    # Make pylint not complaining about unused arguments.
    main = zonem.main.Main(**ctx.obj)
    try:
        main.lint()
    finally:
        main.cleanup()
