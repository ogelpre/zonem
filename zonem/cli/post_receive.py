"""post-receive hook for git"""

import click

import zonem.main


@click.command(short_help='post-receive hook for git')
@click.pass_context
def cli(ctx):
    """post-receive hook"""

    main = zonem.main.Main(**ctx.obj)
    try:
        main.lint()
        main.export()
    finally:
        main.cleanup()
