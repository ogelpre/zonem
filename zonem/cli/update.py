"""update hook for git"""

import os

import click

import zonem.main


@click.command(short_help='update hook for git')
@click.pass_context
@click.argument('ref')
@click.argument('old_rev')
@click.argument('new_rev')
def cli(ctx, ref, old_rev, new_rev):
    """update hook"""

    _ = old_rev

    if 'GIT_DIR' not in os.environ:
        raise RuntimeError('This script hould only be invoked by git.')

    if ref == 'refs/heads/master':
        main = zonem.main.Main(rev=new_rev, **ctx.obj)
        try:
            main.lint()
        finally:
            main.cleanup()
