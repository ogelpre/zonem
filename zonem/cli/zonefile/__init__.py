# pylint: disable=C0114, W0613

import click

from .._utils import DynamicMultiCommandFactory


DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, short_help='zone file import or export')
@click.pass_context
def cli(ctx):
    """zone file import or export

    See subcommands for further informations.
    """
