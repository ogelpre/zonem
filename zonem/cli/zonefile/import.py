"""Export zone files"""

import click

import zonem.main


@click.command(short_help='Import zone files')
@click.pass_context
@click.option('-o', '--origin', type=str, default=None, help='origin for zone file')
@click.option('--host', is_flag=True, default=False, help='combine AAAA and A into a host')
@click.argument('zonefile', type=click.File())
def cli(ctx, origin, zonefile, host):
    """Import zone file"""

    main = zonem.main.Main(**ctx.obj)
    main.import_zone(zonefile, origin, host)
