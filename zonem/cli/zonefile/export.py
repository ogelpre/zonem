"""Export zone files"""

import click

import zonem.main


@click.command(short_help='Export zone files')
@click.pass_context
@click.option('-s', '--suffix', type=str, default=None, help='suffix for zone files')
@click.argument('output_dir', type=click.Path(exists=True, file_okay=False, writable=True, resolve_path=True))
def cli(ctx, suffix, output_dir):
    """Export zone files"""

    main = zonem.main.Main(**ctx.obj)
    main.lint()
    main.export_zone(output_dir, suffix)
