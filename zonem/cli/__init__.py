# pylint: disable=C0114, W0613

import logging

import click
import click_log

from ._utils import DynamicMultiCommandFactory

LOGGER = logging.getLogger(__name__)
click_log.basic_config(LOGGER)

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, context_settings=CONTEXT_SETTINGS)
@click.pass_context
@click_log.simple_verbosity_option(LOGGER)
@click.option('--config', type=click.Path(exists=True), default=None, help='Path to git config.')
@click.option('--secrets', type=click.Path(exists=True), default=None,
              help='File with secrets, which will be inserted in git config.')
@click.option('--bare/--no-bare', default=None, help='Overwrite bare repo detection')
def cli(ctx, config, secrets, bare):
    """zone manager - extensible zone file management with git

    See subcommands for further informations.
    """

    if ctx.obj is None:
        ctx.obj = {}
    if config is None:
        ctx.obj['config'] = None
    else:
        ctx.obj['config'] = click.format_filename(config)
    if secrets is None:
        ctx.obj['secrets'] = None
    else:
        ctx.obj['secrets'] = click.format_filename(secrets)
    ctx.obj['bare'] = bare
