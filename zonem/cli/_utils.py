"""CLICK utils"""

import importlib
import pathlib

import click


class DynamicMultiCommandFactory:
    """Factory for creating a dynamic MultiCommand"""

    def create(self, path, package):
        """Create and return DynamicMulitCommand class

        :param path: search path
        :param package: parents package name
        :type path: type str
        :type package: type str
        :return: Returns a DynamicMultiComman class
        :rtype: class
        """

        plugin_path = pathlib.Path(path).parent

        class DynamicMultiCommand(click.MultiCommand):
            """Creates dynamically multi commands by crawling a directory."""

            def list_commands(self, ctx):
                """Returns a list of subcommand names in the order they should appear."""

                commands = []
                for _path in plugin_path.iterdir():
                    if _path.name.startswith('_'):
                        continue
                    if _path.is_file() and _path.name.endswith('.py'):
                        commands.append(_path.name[:-3])
                    elif _path.is_dir():
                        commands.append(_path.name)
                commands = list([c.replace('_', '-') for c in commands])
                commands.sort()
                return commands

            def get_command(self, ctx, cmd_name):
                """Given a context and a command name, this returns a :class:`Command` object if it exists or returns `None`."""

                commands = self.list_commands(ctx)
                command_candidates = [c for c in commands if c.startswith(cmd_name)]

                if len(command_candidates) == 1:
                    command = command_candidates[0]
                elif len(command_candidates) > 1:
                    ctx.fail("Too many command matches: {matches}".format(
                        matches=', '.join(sorted(command_candidates))))
                else:
                    command = None

                if command is None:
                    return_command = None
                else:
                    try:
                        command_module = importlib.import_module(".{name}".format(name=command.replace('-', '_')), package)
                        return_command = command_module.cli
                    except ModuleNotFoundError:
                        return_command = None

                return return_command

        return DynamicMultiCommand
