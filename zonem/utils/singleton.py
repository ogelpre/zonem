"""Singleton metaclass"""


class MetaSingleton(type):
    """Singeleton metaclass"""
    instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls.instances:
            cls.instances[cls] = super(MetaSingleton, cls).__call__(*args, **kwargs)
        elif args or kwargs:
            raise ValueError("Singelton is already created. Arguments are not supported.")
        return cls.instances[cls]


class Singleton(metaclass=MetaSingleton):
    """Singleton class"""

    def reset(self):
        """Reset singleton"""
        raise NotImplementedError


def reset(cls=None):
    """Reset singleton metaclass."""
    if cls is None:
        for _, instance in MetaSingleton.instances.items():
            instance.reset()
    else:
        MetaSingleton.instances[cls].reset()
