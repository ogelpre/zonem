# pylint: disable=R0914
"""Small snippets which are reusaable inside the project"""

import os
import pathlib
import tempfile

import dns.zone

import zonem.config
import zonem.dns
import zonem.utils


class Main():
    """Initialization"""

    def __init__(self, config=None, secrets=None, rev=None, bare=None):
        git = zonem.config.git.GIT()
        git.load(config, secrets)
        self._config = git.config
        if bare is None and self._config.bare or bare:
            self._temp_dir = tempfile.TemporaryDirectory()
            self._config.base_dir = self._temp_dir.name
            checkout = True
        else:
            self._temp_dir = None
            checkout = False
        if rev is None:
            self._rev = 'master'
        else:
            self._rev = rev
        if checkout:
            zonem.config.git.run_git(
                ['git', 'checkout', '-f', self._rev],
                env=dict(os.environ, GIT_WORK_TREE=str(self._config.base_dir)))
        self._zone_list = zonem.dns.zone.collect_zones(self._config.base_dir, self._config)
        self._ptr = False
        self._validate = False

    def cleanup(self):
        """Cleanup files"""
        if hasattr(self._temp_dir, 'cleanup'):
            self._temp_dir.cleanup()

    @property
    def config(self):
        """Return config"""
        return self._config

    @property
    def zones(self):
        """Return zones"""
        return self._zone_list

    def ptr(self):
        """Create ptr"""
        if self._ptr:
            raise RuntimeError('PTR already created')
        ptr_generator = zonem.post_processing.ptr.PTRGenerator(self._zone_list)
        ptr_generator.run()
        self._ptr = True

    def validate(self):
        """Validate zones"""
        if self._validate:
            raise RuntimeError('Zones already validated')
        target_validator = zonem.post_processing.target.Validator(self._zone_list)
        target_validator.run(self._config.validate_external)
        self._validate = True

    def lint(self):
        """Lint zones"""
        if not self._ptr:
            self.ptr()
        if not self._validate:
            self.validate()

    def export_zone(self, output_dir, suffix=None):
        """Export zones to directory."""
        self._zone_list.write(output_dir, suffix)

    def import_zone(self, zonefile, origin=None, combine_host=None):
        """Import zone file."""
        if origin is None:
            origin = pathlib.Path(zonefile.name).name
        zone = dns.zone.from_file(zonefile, dns.name.from_text(origin))
        zone_path = self._config.base_dir.joinpath(zone.origin.to_text(True))
        for label, node in zone.nodes.items():
            if label.derelativize(zone.origin) == zone.origin:
                node_path = zone_path
            else:
                node_path = zone_path.joinpath(*reversed(label.to_text().split('.')))
            node_path.mkdir(parents=True)
            rdatasets = {}
            for rdataset in node.rdatasets:
                ttl = rdataset.ttl
                rdtype = dns.rdatatype.to_text(rdataset.rdtype)
                rdatas = map(lambda o: o.to_text(), rdataset.items)
                lines = ["{} {} {}".format(ttl, rdtype, rdata) for rdata in rdatas]
                rdatasets[rdtype] = lines
            if combine_host:
                lines = rdatasets.pop('AAAA', []) + rdatasets.pop('A', [])
                if lines:
                    rdatasets['HOST'] = lines
            for rdtype, lines in rdatasets.items():
                with node_path.joinpath(rdtype.lower()).open('w') as stream:
                    data = os.linesep.join(lines)
                    stream.write(data)

    def export(self):
        """Export zones"""
        for zone in self._zone_list:
            for exporter in self._config[zone.origin].exporter:
                exporter.export(zone)
